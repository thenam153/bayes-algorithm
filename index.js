const express   = require('express')
const cors      = require('cors')
const jwt       = require('jsonwebtoken')
const passport  = require('passport')
const path      = require('path')
const bodyParse = require('body-parser')
const app       = express()

app.use(cors())
app.use(bodyParse.json())
app.use(bodyParse.urlencoded({ extended: true })) // use with querystring or qs 
app.use(passport.initialize())
app.use(passport.session())

app.listen(3000, function() {
    console.log('Success!')
})


// create user
// create model | user
 
    // refresh token

// put data | key | token 

// train data | key | token

// predict | key | token


// cung cấp api trên put data