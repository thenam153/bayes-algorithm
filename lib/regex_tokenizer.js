
const _ = require('lodash');

const digit = "\\d+([\\.,_]\\d+)+"
const email = "(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$)"
const web = "^(http[s]?://)?(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\\(\\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+$"
const word = "[aáàạảãăắằặẳẵâấầậẩẫbcdđeéèẹẻẽêếềệểễfghiíìịỉĩjklmnoóòọỏõôồốộổỗơớờợởỡpqrstuúùụủũưứừựửữvwxyýỳỵỷỹzAÁÀẠẢÃĂẮẰẶẲẴÂẤẦẬẨẪBCDĐEÉÈẸẺẼÊẾỀỆỂỄFGHIÍÌỊỈĨJKLMNOÓÒỌỎÕÔỐỒỘỔỖƠỚỜỢỞỠPQRSTUÚÙỤỦŨƯỨỪỰỬỮVWXYÝỲỴỶỸZ0-9]+"
const non_word = "[^aáàạảãăắằặẳẵâấầậẩẫbcdđeéèẹẻẽêếềệểễfghiíìịỉĩjklmnoóòọỏõôồốộổỗơớờợởỡpqrstuúùụủũưứừựửữvwxyýỳỵỷỹzAÁÀẠẢÃĂẮẰẶẲẴÂẤẦẬẨẪBCDĐEÉÈẸẺẼÊẾỀỆỂỄFGHIÍÌỊỈĨJKLMNOÓÒỌỎÕÔỐỒỘỔỖƠỚỜỢỞỠPQRSTUÚÙỤỦŨƯỨỪỰỬỮVWXYÝỲỴỶỸZ0-9\\s]"

const specials = ["==>", "->", "\\.\\.\\.", ">>"]
const datetime = [
    "\\d{1,2}\\/\\d{1,2}(\\/\\d+)?",
    "\\d{1,2}-\\d{1,2}(-\\d+)?",
]
const abbreviations = [
    "[A-ZĐ]+\\.",
    "Tp\\.",
    "Mr\\.", "Mrs\\.", "Ms\\.",
    "Dr\\.", "ThS\\."
]

const patterns = _.concat(abbreviations, specials, [web, email], datetime, [digit, non_word, word])

class RegexTokenize {
    constructor() {
        this._pattern = `(${ patterns.join('|') })`;
    }
    trim(array) {
        while(!array[array.lenth - 1]) {
            array.pop();
        }
        while(!array[0]) {
            array.shift();
        }
        return array;
    }

    splitText(text) {
        let regex = new RegExp(this._pattern, 'g')
        text = text.replace(/\s+/, ' ');
        return text.match(regex) || [];
    }
    joinText(text, separator) {
        return this.splitText(text).join(separator || ' ');
    }


}

module.exports = new RegexTokenize();