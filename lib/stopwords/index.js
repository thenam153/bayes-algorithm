var fs = require('fs');
var path = require('path');

function stopwordsDash() {
    var words = fs.readFileSync(path.join(__dirname + '/stopwords-dash.txt'), 'utf-8');
    return words.split('\n').filter(word => word)
}

function stopwords() {
    var words = fs.readFileSync(path.join(__dirname + '/stopwords.txt'), 'utf-8');
    return words.split('\n').filter(word => word)
}

module.exports.stopwordsDash = stopwordsDash;
module.exports.stopwords = stopwords;